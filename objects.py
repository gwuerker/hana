import os
import json


class Job:
	def __init__(self, job_name, num_procs, u_id, g_id, wall_clock_limit, wall_completion_state, queue, queue_time, dispatch_time, start_time, completion_time, nodes):	
		self.job_name = job_name
		self.num_procs = int(num_procs)
		self.u_id = u_id
		self.g_id = g_id
		self.wall_clock_limit = int(wall_clock_limit)
		self.wall_completion_state = wall_completion_state
		self.queue = queue
		self.queue_time = int(queue_time)
		self.dispatch_time = int(dispatch_time)
		self.start_time = int(start_time)
		self.completion_time = int(completion_time)
		self.nodes = Job.split_nodes_string(nodes)
		self.num_nodes = len(self.nodes)
		self.procs_per_node = self.num_procs / self.num_nodes
		self.run_time = self.completion_time - self.start_time

	def to_string(self):
		 return json.dumps(self, default=lambda o: o.__dict__)

	@staticmethod
	def split_nodes_string(nodes_string):
		nodes = []
		for node in nodes_string.split(':'):
			nodes.append(node.rstrip('.'))
	
		return nodes

	@staticmethod
	def get_user_jobs(jobs, u_id):
		user_jobs = []
		for job in jobs:
			if job.u_id == u_id:
				user_jobs.append(job)

		return user_jobs

class Node:
	def __init__(self, host_name):
		self.host_name = host_name
		self.num_jobs = 0
		self.job_run_time = 0
		self.procs_run_time = 0
		self.procs_used = 0
		self.jobs = []
	
	def attribute_job(self, job):
		self.num_jobs += 1
		self.job_run_time += job.run_time
		self.procs_run_time += job.num_procs * job.run_time
		self.procs_used += job.procs_per_node
		self.jobs.append(job)
	
	def to_string(self):
		return json.dumps(self, default=lambda o: o.__dict__)

	@staticmethod
	def get_node(nodes, host_name):
		for node in nodes:
			if node.host_name == host_name:
				return node

		return None

class User:
	def __init__(self, u_id):
		self.u_id = u_id
		self.num_jobs = 0
		self.job_run_time = 0
		self.procs_run_time = 0
		self.procs_used = 0
		self.jobs = []

	def attribute_job(self, job):
		self.num_jobs += 1
		self.job_run_time += job.run_time
		self.procs_run_time += job.num_procs * job.run_time
		self.procs_used += job.num_procs
		self.jobs.append(job)

	def to_string(self):
		return json.dumps(self, default=lambda o: o.__dict__)

	@staticmethod
	def get_user(users, u_id):
		for user in users:
			if user.u_id == u_id:
				return user

		return None
