import os
import json
import util

from datetime import date
from objects import User, Job, Node

def jobs_to_nodes(jobs):
	nodes = []
	for job in jobs:
		for host_name in job.nodes:
			node = Node.get_node(nodes, host_name)
			if node is None:
				node = Node(host_name)
				nodes.append(node)			
	
			node.attribute_job(job)

	return nodes

def jobs_to_users(jobs):
	users = []
	for job in jobs:
		user = User.get_user(users, job.u_id)
		if user is None:
			user = User(job.u_id)
			users.append(user)			
	
		user.attribute_job(job)

	return users

def file_to_jobs(file_path):
	jobs = []
	if os.path.isfile(file_path):
		file = open(file_path, 'r')
		for job_string in file:
			job_string_array = job_string.split()
			if len(job_string_array) == 44:
				job = Job(job_string_array[0], job_string_array[2], job_string_array[3], job_string_array[4], job_string_array[5], job_string_array[6], job_string_array[7], job_string_array[8], job_string_array[9], job_string_array[10], job_string_array[11], job_string_array[37])
				jobs.append(job) 	
	return jobs

def files_to_jobs_in_range(root_path, start, end):
	jobs = []
	for file_name in os.listdir(root_path):
		file_date = util.file_name_to_date(file_name)
		if start <= file_date and file_date <= end:
			file_path = os.path.join(root_path, file_name)
			jobs += file_to_jobs(file_path)
	
	return jobs

