import tornado.web
import data_conversions
import util

from datetime import date, datetime, timedelta
from objects import User, Node, Job

class HomeHandler(tornado.web.RequestHandler):
	def get(self):
		self.render('templates/home.tmpl')

class UserHandler(tornado.web.RequestHandler):
	def get(self, user_name=None):
		start = get_start_date(self)
		end = get_end_date(self)
		
		jobs = data_conversions.files_to_jobs_in_range('/var/maui/stats/', start, end)
		users = data_conversions.jobs_to_users(jobs)
	
		sorting_attribute = self.get_argument('sort_by', None, True)
		if sorting_attribute is not None:
			users = util.sort_users_by(users, sorting_attribute)

		if user_name:
			user = User.get_user(users, user_name)
			if user is None:
				user = User(user_name)
				
			params = {
				'user'		: user,
				'start'		: start,
				'end'		: end,
				'format_time'	: util.format_time,
				'time_to_date'	: util.time_to_date
			}
			self.render('templates/user.tmpl', **params)
	
		else:
			params = {
				'users' 		: users,
				'start' 		: start,
				'end'   		: end,
				'format_time'		: util.format_time,
				'current_url'		: self.request.uri,
				'build_sorted_url'	: util.build_sorted_url
			}
			self.render('templates/users.tmpl', **params)	

class NodeHandler(tornado.web.RequestHandler):
	def get(self, node_name=None):
		start = get_start_date(self)
		end = get_end_date(self)
		
		jobs = data_conversions.files_to_jobs_in_range('/var/maui/stats/', start, end)
		nodes = data_conversions.jobs_to_nodes(jobs)

		sorting_attribute = self.get_argument('sort_by', None, True)
		if sorting_attribute is not None:
			nodes = util.sort_nodes_by(nodes, sorting_attribute)

		if node_name:
			node = Node.get_node(nodes, node_name)
			if node is None:
				node = Node(host_name)

			params = {
				'node'		: node,
				'start'		: start,
				'end'		: end,
				'format_time'	: util.format_time,
				'time_to_date'	: util.time_to_date,
			}
			self.render('templates/node.tmpl', **params)
	
		else:
			params = {
				'nodes' 		: nodes,
				'start' 		: start,
				'end'   		: end,
				'format_time'		: util.format_time,
				'current_url'		: self.request.uri,
				'build_sorted_url' 	: util.build_sorted_url
			}
			self.render('templates/nodes.tmpl', **params)	

def get_start_date(self):
	start_year = self.get_argument('start_year', None, True)	
	start_month = self.get_argument('start_month', None, True)
	start_day = self.get_argument('start_day', None, True)

	end_year = self.get_argument('end_year', None, True)
	end_month = self.get_argument('end_month', None, True)
	end_day = self.get_argument('end_day', None, True)

	if start_year is None or start_month is None or start_day is None or end_year is None or end_month is None or end_day is None:
		last_week = (datetime.now() - timedelta(days=7)).date()		
		return date(last_week.year, last_week.month, last_week.day)

	return date(int(start_year), int(start_month), int(start_day))

	
def get_end_date(self):
	start_year = self.get_argument('start_year', None, True)	
	start_month = self.get_argument('start_month', None, True)
	start_day = self.get_argument('start_day', None, True)

	end_year = self.get_argument('end_year', None, True)
	end_month = self.get_argument('end_month', None, True)
	end_day = self.get_argument('end_day', None, True)

	if start_year is None or start_month is None or start_day is None or end_year is None or end_month is None or end_day is None:
		today = datetime.now()
		return date(today.year, today.month, today.day)

	return date(int(end_year), int(end_month), int(end_day))	
