from datetime import date
from time import strptime, strftime, localtime
from objects import User, Node

def file_name_to_date(file_name):
	file_name_array = file_name.split('_')
	month = strptime(file_name_array[1], '%b').tm_mon
	day = int(file_name_array[2])
	year = int(file_name_array[3])
	return date(year, month, day)

def format_time(seconds):
	time = seconds
	#seconds
	if time < 60:
		return '{} seconds'.format(time)

	#minutes
	time /= 60
	if time < 60:
		return '{} minutes'.format(time)

	#hours
	time /= 60
	if time < 24:
		return '{} hours'.format(time)

	#days
	time /= 24
	return '{} days'.format(time)

def time_to_date(epoch_time):
	return strftime('%Y/%m/%d %H:%M:%S', localtime(epoch_time))

def sort_users_by(users, attribute):
	sorted_users = sorted(users, key=lambda user: getattr(user, attribute), reverse=True)
	return sorted_users

def sort_nodes_by(nodes, attribute):
	sorted_nodes = sorted(nodes, key=lambda node: getattr(node, attribute), reverse=True)
	return sorted_nodes

def build_sorted_url(current_url, sort_by):
	if '?' in current_url:
		return '{0}&sort_by={1}'.format(current_url, sort_by)

	return '{0}?sort_by={1}'.format(current_url, sort_by)


