import sys
    
from tornado.options import define, options
from application import HanaApplication

# Main execution

if __name__ == '__main__':
	try:
		hana_application = HanaApplication()
		hana_application.run()
	except KeyboardInterrupt:
		sys.exit(0)
