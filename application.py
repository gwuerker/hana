import logging

import tornado.ioloop
import tornado.web

from handler import UserHandler, NodeHandler, HomeHandler

class HanaApplication(tornado.web.Application):

	def __init__(self):
		tornado.web.Application.__init__(self)	
		self.add_handlers('', [
			(r'/', HomeHandler),
			(r'/user/([^/]*)', UserHandler),
			(r'/node/([^/]*)', NodeHandler)
		])


	def run(self):
		self.listen(52080)
		tornado.ioloop.IOLoop.instance().start()


